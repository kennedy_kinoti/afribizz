<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {


	public function index()
        {
                $header_data['pagename'] = 'Afribusiness LLP | Home';
                $this->load->view('global/header', $header_data, FALSE);
                // $this->load->view('home/styles');
                // $this->load->view('global/menu');
                $this->load->view('home/index');
                $this->load->view('global/footer');
                // $this->load->view('home/scripts');
                
        }
}
