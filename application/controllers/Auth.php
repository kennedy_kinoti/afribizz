<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller{

	function __construct()
	{
		parent::__construct();
	}

	public function login()
	{
		$header_data['pagename'] = 'AfriBusiness | Login';
		$this->load->view('auth/template/header', $header_data, FALSE);
		$this->load->view('auth/login/index', FALSE);
		$this->load->view('auth/template/footer', FALSE);
	}
}